<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminKeywordController extends Controller
{
    public function index()
    {
        return view('admin.keyword.index');
    }
}
