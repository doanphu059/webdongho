<?php

use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\Admin\AdminKeywordController;
Route::group(['prefix' => 'api-admin', 'namespace' => 'Admin'], function(){
    Route::get('/', function (){
        return view('admin/index');
    });
    Route::group(['prefix' => 'category'], function (){
        Route::get('',[AdminCategoryController::class,"index"])->name('admin.category.index');
        Route::get('create',[AdminCategoryController::class,"create"])->name('admin.category.create');
        Route::post('create',[AdminCategoryController::class,"store"]);

        Route::get('update/{id}',[AdminCategoryController::class,"edit"])->name('admin.category.update');
        Route::post('update/{id}',[AdminCategoryController::class,"update"]);


        Route::get('active/{id}',[AdminCategoryController::class,"active"])->name('admin.category.active');
        Route::get('hot/{id}',[AdminCategoryController::class,"hot"])->name('admin.category.hot');
        Route::get('delete/{id}',[AdminCategoryController::class,"delete"])->name('admin.category.delete');

    });
    Route::group(['prefix' => 'keyword'], function (){
        Route::get('',[AdminKeywordController::class,"index"])->name('admin.keyword.index');
        Route::get('create',[AdminKeywordController::class,"create"])->name('admin.keyword.create');
        Route::post('create',[AdminKeywordController::class,"store"]);

        Route::get('update/{id}',[AdminKeywordController::class,"edit"])->name('admin.keyword.update');
        Route::post('update/{id}',[AdminKeywordController::class,"update"]);


        Route::get('delete/{id}',[AdminKeywordController::class,"delete"])->name('admin.keyword.delete');

    });

});

